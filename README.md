# Ray Tracer #

This ray tracer was built in Rust using [Ray Tracing in One Weekend](https://raytracing.github.io/books/RayTracingInOneWeekend.html) as a guide.

Here's an example image:
![An image generated by this ray tracer](example.png)
