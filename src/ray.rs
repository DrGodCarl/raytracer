use crate::vec::{Float, Point3, Vec3};

pub struct Ray(pub Point3, pub Vec3);

impl Ray {
    pub fn zero() -> Ray {
        Ray(Point3::zero(), Vec3::zero())
    }

    #[inline]
    pub fn origin(&self) -> Point3 {
        self.0
    }

    #[inline]
    pub fn direction(&self) -> Point3 {
        self.1
    }

    pub fn at(&self, t: Float) -> Point3 {
        self.origin() + t * self.direction()
    }
}
