use crate::util::{random, random_range};
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

pub type Float = f64;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Vec3(pub Float, pub Float, pub Float);
pub type Point3 = Vec3;
pub type Color = Vec3;

impl Vec3 {
    pub fn new(x: Float, y: Float, z: Float) -> Self {
        Vec3(x, y, z)
    }

    pub fn zero() -> Self {
        Vec3(0.0, 0.0, 0.0)
    }

    pub fn random() -> Self {
        Vec3(random(), random(), random())
    }

    pub fn random_range(min: Float, max: Float) -> Self {
        Vec3(
            random_range(min, max),
            random_range(min, max),
            random_range(min, max),
        )
    }

    pub fn random_in_unit_sphere() -> Self {
        loop {
            let p = Self::random_range(-1.0, 1.0);
            if p.length_squared() > 1.0 {
                continue;
            }
            return p;
        }
    }

    pub fn random_unit_vector() -> Self {
        // TODO - use std::f64::consts::TAU when available
        let a = random_range(0.0, 2.0 * std::f64::consts::PI);
        let z = random_range(-1.0, 1.0);
        let r = (1.0 - z.powi(2)).sqrt();
        Self(r * a.cos(), r * a.sin(), z)
    }

    pub fn random_in_hemisphere(normal: &Self) -> Self {
        let result = Self::random_in_unit_sphere();
        if result.dot(normal) > 0.0 {
            return result;
        }
        return -result;
    }

    pub fn random_unit_in_disk() -> Self {
        loop {
            let p = Vec3(random_range(-1.0, 1.0), random_range(-1.0, 1.0), 0.0);
            if p.length_squared() >= 1.0 {
                continue;
            }
            return p;
        }
    }

    pub fn apply(&self, func: impl Fn(f64) -> f64) -> Self {
        Vec3(func(self.x()), func(self.y()), func(self.z()))
    }

    #[inline]
    pub fn x(&self) -> Float {
        self.0
    }

    #[inline]
    pub fn y(&self) -> Float {
        self.1
    }

    #[inline]
    pub fn z(&self) -> Float {
        self.2
    }

    #[inline]
    pub fn length(&self) -> Float {
        self.length_squared().sqrt()
    }

    #[inline]
    pub fn length_squared(&self) -> Float {
        self.x().powi(2) + self.y().powi(2) + self.z().powi(2)
    }

    #[inline]
    pub fn dot(&self, other: &Self) -> Float {
        self.x() * other.x() + self.y() * other.y() + self.z() * other.z()
    }

    pub fn reflected_about(&self, normal: &Self) -> Self {
        *self - 2.0 * self.dot(normal) * normal
    }

    pub fn refracted_about(&self, normal: &Self, etai_over_etat: Float) -> Self {
        let cos_theta = (-self).dot(normal);
        let r_out_perp = etai_over_etat * (*self + cos_theta * normal);
        let r_out_parallel = -(1.0 - r_out_perp.length_squared()).abs().sqrt() * normal;
        return r_out_perp + r_out_parallel;
    }

    #[inline]
    pub fn cross(&self, other: &Self) -> Self {
        Self(
            self.y() * other.z() - self.z() * other.y(),
            self.z() * other.x() - self.x() * other.z(),
            self.x() * other.y() - self.y() * other.x(),
        )
    }

    pub fn unit_vector(self) -> Self {
        self / self.length()
    }
}

impl Color {
    #[inline]
    pub fn r(&self) -> Float {
        self.x()
    }

    #[inline]
    pub fn g(&self) -> Float {
        self.y()
    }

    #[inline]
    pub fn b(&self) -> Float {
        self.z()
    }
}

impl Neg for Vec3 {
    type Output = Self;
    fn neg(self) -> Self {
        Self(-self.x(), -self.y(), -self.z())
    }
}

impl Neg for &Vec3 {
    type Output = Vec3;
    fn neg(self) -> Vec3 {
        -(*self)
    }
}

impl Add for Vec3 {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self(
            self.x() + other.x(),
            self.y() + other.y(),
            self.z() + other.z(),
        )
    }
}

impl AddAssign for Vec3 {
    fn add_assign(&mut self, other: Self) {
        *self = *self + other;
    }
}

impl Sub for Vec3 {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        self + (-other)
    }
}

impl SubAssign for Vec3 {
    fn sub_assign(&mut self, other: Self) {
        *self = *self - other;
    }
}

impl Mul<Float> for Vec3 {
    type Output = Self;

    fn mul(self, scalar: Float) -> Self {
        Self(self.x() * scalar, self.y() * scalar, self.z() * scalar)
    }
}

impl Mul<Float> for &Vec3 {
    type Output = Vec3;

    fn mul(self, scalar: Float) -> Vec3 {
        *self * scalar
    }
}

impl Mul<Vec3> for Float {
    type Output = Vec3;

    fn mul(self, vector: Vec3) -> Vec3 {
        vector * self
    }
}

impl Mul<&Vec3> for Float {
    type Output = Vec3;

    fn mul(self, vector: &Vec3) -> Vec3 {
        *vector * self
    }
}

impl Mul<Vec3> for Vec3 {
    type Output = Vec3;

    fn mul(self, other: Self) -> Self {
        Self(
            self.x() * other.x(),
            self.y() * other.y(),
            self.z() * other.z(),
        )
    }
}

impl MulAssign<Float> for Vec3 {
    fn mul_assign(&mut self, scalar: Float) {
        *self = *self * scalar;
    }
}

impl MulAssign<Vec3> for Vec3 {
    fn mul_assign(&mut self, other: Vec3) {
        *self = *self * other;
    }
}

impl Div<Float> for Vec3 {
    type Output = Self;

    fn div(self, scalar: Float) -> Self {
        self * (1.0 / scalar)
    }
}

impl DivAssign<Float> for Vec3 {
    fn div_assign(&mut self, scalar: Float) {
        *self = *self / scalar;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_dot() {
        let a = Vec3(9.0, 2.0, 7.0);
        let b = Vec3(4.0, 8.0, 10.0);

        let dot = a.dot(&b);
        assert_eq!(dot, 122.0);
    }

    #[test]
    fn test_cross() {
        let a = Vec3(2.0, 3.0, 4.0);
        let b = Vec3(5.0, 6.0, 7.0);

        let dot = a.cross(&b);
        assert_eq!(dot, Vec3(-3.0, 6.0, -3.0));
    }

    #[test]
    fn test_length_squared() {
        let a = Vec3(2.0, 10.0, 11.0);

        let length_sqared = a.length_squared();
        assert_eq!(length_sqared, 225.0);
    }

    #[test]
    fn test_length() {
        let a = Vec3(2.0, 10.0, 11.0);

        let length_sqared = a.length();
        assert_eq!(length_sqared, 15.0);
    }

    #[test]
    fn test_negation() {
        let a = Vec3(1.0, 2.0, 3.0);

        let b = -a;
        assert_eq!(Vec3(-1.0, -2.0, -3.0), b);
    }

    #[test]
    fn test_vec3_addition() {
        let a = Vec3(1.0, 2.0, 3.0);
        let b = Vec3(2.0, 4.0, 6.0);
        let zero = Vec3::zero();

        let c = a + zero;
        assert_eq!(a, c);

        let c = a + b;
        assert_eq!(Vec3(3.0, 6.0, 9.0), c);
    }

    #[test]
    fn test_vec3_mutating_addition() {
        let mut a = Vec3(1.0, 2.0, 3.0);
        let b = Vec3(2.0, 4.0, 6.0);

        a += b;
        assert_eq!(Vec3(3.0, 6.0, 9.0), a);
    }

    #[test]
    fn test_vec3_subtraction() {
        let a = Vec3(3.0, 6.0, 9.0);
        let b = Vec3(1.0, 2.0, 3.0);
        let zero = Vec3::zero();

        let c = a - zero;
        assert_eq!(a, c);

        let c = a - b;
        assert_eq!(Vec3(2.0, 4.0, 6.0), c);
    }

    #[test]
    fn test_vec3_mutating_subtraction() {
        let mut a = Vec3(3.0, 6.0, 9.0);
        let b = Vec3(1.0, 2.0, 3.0);

        a -= b;
        assert_eq!(Vec3(2.0, 4.0, 6.0), a);
    }

    #[test]
    fn test_vec3_multiplication() {
        let a = Vec3(1.0, 2.0, 3.0);
        let scalar = 2.0;

        let b = a * scalar;

        assert_eq!(Vec3(2.0, 4.0, 6.0), b);

        let b = scalar * a;
        assert_eq!(Vec3(2.0, 4.0, 6.0), b);
    }

    #[test]
    fn test_vec3_mutating_multiplication() {
        let mut a = Vec3(1.0, 2.0, 3.0);
        let b = 3.0;

        a *= b;
        assert_eq!(Vec3(3.0, 6.0, 9.0), a);
    }

    #[test]
    fn test_multiply_two_vectors() {
        let a = Vec3(1.0, 2.0, 3.0);
        let b = Vec3(2.0, 3.0, 4.0);

        let c = a * b;
        assert_eq!(c, Vec3(2.0, 6.0, 12.0));
    }

    #[test]
    fn test_vec3_division() {
        let a = Vec3(2.0, 4.0, 6.0);
        let scalar = 2.0;

        let b = a / scalar;
        assert_eq!(Vec3(1.0, 2.0, 3.0), b);
    }

    #[test]
    fn test_vec3_mutating_division() {
        let mut a = Vec3(2.0, 4.0, 6.0);
        let scalar = 2.0;

        a /= scalar;
        assert_eq!(Vec3(1.0, 2.0, 3.0), a);
    }
}
