use rand::prelude::*;

#[inline]
pub fn clamp(x: f64, min: f64, max: f64) -> f64 {
    x.max(min).min(max)
}

pub fn random() -> f64 {
    let mut rng = thread_rng();
    rng.gen()
}

pub fn random_range(min: f64, max: f64) -> f64 {
    let mut rng = thread_rng();
    rng.gen_range(min, max)
}
