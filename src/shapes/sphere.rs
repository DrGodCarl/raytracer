use std::sync::Arc;

use super::hittable::{HitRecord, Hittable};
use crate::materials::Material;
use crate::ray::Ray;
use crate::vec::Point3;

pub struct Sphere {
    pub center: Point3,
    pub radius: f64,
    pub material: Arc<dyn Material + Sync + Send>,
}

impl Hittable for Sphere {
    fn hit(&self, ray: &Ray, t_min: f64, t_max: f64, record: &mut HitRecord) -> bool {
        let oc = ray.origin() - self.center;
        let a = ray.direction().length_squared();
        let half_b = oc.dot(&ray.direction());
        let c = oc.length_squared() - self.radius.powi(2);
        let discriminant = half_b * half_b - a * c;
        if discriminant > 0.0 {
            let mut update_record = |t| {
                record.t = t;
                record.point = ray.at(record.t);
                let outward_normal = (record.point - self.center) / self.radius;
                record.set_face_normal(ray, &outward_normal);
                record.material = Some(self.material.clone())
            };

            let root = discriminant.sqrt();

            let temp = (-half_b - root) / a;
            if temp < t_max && temp > t_min {
                update_record(temp);
                return true;
            }

            let temp = (-half_b + root) / a;
            if temp < t_max && temp > t_min {
                update_record(temp);
                return true;
            }
        }
        return false;
    }
}
