use super::hittable::{HitRecord, Hittable};

impl Hittable for Vec<Box<dyn Hittable + Sync + Send>> {
    fn hit(
        &self,
        ray: &crate::ray::Ray,
        t_min: f64,
        t_max: f64,
        record: &mut super::hittable::HitRecord,
    ) -> bool {
        let mut temp_record = HitRecord::default();
        let mut hit_something = false;
        let mut closest_so_far = t_max;

        for obj in self {
            if obj.hit(ray, t_min, closest_so_far, &mut temp_record) {
                hit_something = true;
                closest_so_far = temp_record.t;
                *record = temp_record.clone();
            }
        }
        return hit_something;
    }
}
