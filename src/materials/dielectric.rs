use super::Material;
use crate::ray::Ray;
use crate::shapes::hittable::HitRecord;
use crate::util::random;
use crate::vec::Color;

pub struct Dielectric {
    pub refractive_index: f64,
}

impl Material for Dielectric {
    fn scatter(
        &self,
        ray: &Ray,
        hit_record: &HitRecord,
        attenuation: &mut Color,
        scattered: &mut Ray,
    ) -> bool {
        *attenuation = Color::new(1.0, 1.0, 1.0);
        let etai_over_etat = if hit_record.front_face {
            1.0 / self.refractive_index
        } else {
            self.refractive_index
        };
        let unit_direction = ray.direction().unit_vector();

        let cos_theta = (-unit_direction).dot(&hit_record.normal).min(1.0);
        let sin_theta = (1.0 - cos_theta.powi(2)).sqrt();
        if etai_over_etat * sin_theta > 1.0 || random() < schlick(cos_theta, etai_over_etat) {
            let reflected = unit_direction.reflected_about(&hit_record.normal);
            *scattered = Ray(hit_record.point, reflected);
        } else {
            let refracted = unit_direction.refracted_about(&hit_record.normal, etai_over_etat);
            *scattered = Ray(hit_record.point, refracted);
        }

        return true;
    }
}

fn schlick(cosine: f64, refractive_index: f64) -> f64 {
    let r0 = (1.0 - refractive_index) / (1.0 + refractive_index);
    let r0 = r0.powi(2);
    r0 + (1.0 - r0) * (1.0 - cosine).powi(5)
}
