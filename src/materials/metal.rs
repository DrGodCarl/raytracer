use super::Material;
use crate::ray::Ray;
use crate::shapes::hittable::HitRecord;
use crate::vec::{Color, Vec3};

pub struct Metal {
    pub albedo: Color,
    pub fuzz: f64,
}

impl Material for Metal {
    fn scatter(
        &self,
        ray: &Ray,
        hit_record: &HitRecord,
        attenuation: &mut Color,
        scattered: &mut Ray,
    ) -> bool {
        let reflected = ray
            .direction()
            .unit_vector()
            .reflected_about(&hit_record.normal);
        *scattered = Ray(
            hit_record.point,
            reflected + self.fuzz * Vec3::random_in_unit_sphere(),
        );
        *attenuation = self.albedo;
        return scattered.direction().dot(&hit_record.normal) > 0.0;
    }
}
