pub mod dielectric;
pub mod lambertian;
pub mod metal;

use crate::ray::Ray;
use crate::shapes::hittable::HitRecord;
use crate::vec::Color;

pub trait Material {
    fn scatter(
        &self,
        ray: &Ray,
        hit_record: &HitRecord,
        attenuation: &mut Color,
        scattered: &mut Ray,
    ) -> bool;
}
