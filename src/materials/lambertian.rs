use super::Material;
use crate::ray::Ray;
use crate::shapes::hittable::HitRecord;
use crate::vec::{Color, Vec3};

pub struct Lambertian {
    pub albedo: Color,
}

impl Material for Lambertian {
    fn scatter(
        &self,
        _ray: &crate::ray::Ray,
        hit_record: &HitRecord,
        attenuation: &mut Color,
        scattered: &mut crate::ray::Ray,
    ) -> bool {
        let scatter_direction = hit_record.normal + Vec3::random_unit_vector();
        *scattered = Ray(hit_record.point, scatter_direction);
        *attenuation = self.albedo;
        return true;
    }
}
