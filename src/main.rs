use rayon::prelude::*;

use std::sync::Arc;

use camera::Camera;
use materials::dielectric::Dielectric;
use materials::lambertian::Lambertian;
use materials::metal::Metal;
use ray::Ray;
use shapes::hittable::{HitRecord, Hittable};
use shapes::sphere::Sphere;
use util::{clamp, random, random_range};
use vec::{Color, Point3, Vec3};

pub mod camera;
pub mod materials;
pub mod ray;
pub mod shapes;
pub mod util;
pub mod vec;

fn main() {
    let aspect_ratio = 3.0 / 2.0;
    let image_width = 1200;
    let image_height = (image_width as f64 / aspect_ratio) as i32;
    let samples_per_pixel = 500;
    let max_depth = 50;

    let world = random_scene();

    let look_from = Point3::new(13.0, 2.0, 3.0);
    let look_at = Point3::new(0.0, 0.0, 0.0);
    let view_up = Vec3(0.0, 1.0, 0.0);
    let vertical_field_of_view_in_degrees = 20.0;
    let focus_distance = 10.0;
    let aperture = 0.1;
    let camera = Camera::new(
        look_from,
        look_at,
        view_up,
        vertical_field_of_view_in_degrees,
        aspect_ratio,
        aperture,
        focus_distance,
    );

    println!("P3\n{} {}\n255", image_width, image_height);
    for y in (0..image_height).rev() {
        eprintln!("Scanlines remaining: {}", y);
        (0..image_width)
            .into_par_iter()
            .map(|x| {
                let mut pixel_color = Color::new(0.0, 0.0, 0.0);
                for _ in 0..samples_per_pixel {
                    let u = (x as f64 + random()) / (image_width as f64 - 1.0);
                    let v = (y as f64 + random()) / (image_height as f64 - 1.0);
                    let r = camera.get_ray(u, v);
                    pixel_color += ray_color(&r, &world, max_depth);
                }
                pixel_color
            })
            .collect::<Vec<Color>>()
            .into_iter()
            .for_each(|c| write_color(&c, samples_per_pixel));
    }

    eprintln!("Done.")
}

fn random_scene() -> impl Hittable + Sync + Send {
    let ground = Arc::new(Lambertian {
        albedo: Color::new(0.5, 0.5, 0.5),
    });
    let mut world: Vec<Box<dyn Hittable + Sync + Send>> = vec![Box::new(Sphere {
        center: Point3::new(0.0, -1000.0, 0.0),
        radius: 1000.0,
        material: ground,
    })];
    for a in -11..11 {
        for b in -11..11 {
            let choose_material = random();
            let center = Point3::new(a as f64 + 0.9 * random(), 0.2, b as f64 + 0.9 * random());
            if (center - Point3::new(4.0, 0.2, 0.0)).length() > 0.9 {
                if choose_material < 0.8 {
                    let color = Color::random() * Color::random();
                    let material = Arc::new(Lambertian { albedo: color });
                    world.push(Box::new(Sphere {
                        center,
                        radius: 0.2,
                        material,
                    }))
                } else if choose_material < 0.95 {
                    let color = Color::random_range(0.5, 1.0);
                    let fuzz = random_range(0.0, 0.5);
                    let material = Arc::new(Metal {
                        albedo: color,
                        fuzz,
                    });
                    world.push(Box::new(Sphere {
                        center,
                        radius: 0.2,
                        material,
                    }));
                } else {
                    let material = Arc::new(Dielectric {
                        refractive_index: 1.5,
                    });
                    world.push(Box::new(Sphere {
                        center,
                        radius: 0.2,
                        material,
                    }));
                }
            }
        }
    }

    let glass = Arc::new(Dielectric {
        refractive_index: 1.5,
    });
    world.push(Box::new(Sphere {
        center: Point3::new(0.0, 1.0, 0.0),
        radius: 1.0,
        material: glass,
    }));

    let diffuse = Arc::new(Lambertian {
        albedo: Color::new(0.4, 0.2, 0.1),
    });
    world.push(Box::new(Sphere {
        center: Point3::new(-4.0, 1.0, 0.0),
        radius: 1.0,
        material: diffuse,
    }));

    let metal = Arc::new(Metal {
        albedo: Color::new(0.7, 0.6, 0.5),
        fuzz: 0.0,
    });
    world.push(Box::new(Sphere {
        center: Point3::new(4.0, 1.0, 0.0),
        radius: 1.0,
        material: metal,
    }));

    return world;
}

fn write_color(color: &Color, samples_per_pixel: u16) {
    let scale = 1.0 / (samples_per_pixel as f64);
    let scaled_color = color
        .apply(|c| (c * scale).sqrt())
        .apply(|c| clamp(c, 0.0, 0.999))
        * 256.0;
    println!(
        "{} {} {}",
        scaled_color.r() as u8,
        scaled_color.g() as u8,
        scaled_color.b() as u8
    );
}

fn ray_color(ray: &Ray, world: &dyn Hittable, depth: u16) -> Color {
    if depth <= 0 {
        return Color::zero();
    }
    let mut hit_record = HitRecord::default();
    if world.hit(ray, 0.001, std::f64::INFINITY, &mut hit_record) {
        let mut scattered = Ray::zero();
        let mut attenuation = Color::zero();
        if let Some(material) = &hit_record.material {
            if material.scatter(ray, &hit_record, &mut attenuation, &mut scattered) {
                return attenuation * ray_color(&scattered, world, depth - 1);
            }
        }

        return Color::zero();
    }
    let direction = ray.direction().unit_vector();
    let t = 0.5 * (direction.y() + 1.0);
    (1.0 - t) * Color::new(1.0, 1.0, 1.0) + t * Color::new(0.5, 0.7, 1.0)
}
