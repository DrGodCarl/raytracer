use crate::ray::Ray;
use crate::vec::{Point3, Vec3};

pub struct Camera {
    origin: Point3,
    lower_left_corner: Point3,
    horizontal: Vec3,
    vertical: Vec3,
    basis: OrthonormalBasis,
    lens_radius: f64,
}

pub struct OrthonormalBasis(Vec3, Vec3, Vec3);

impl OrthonormalBasis {
    #[inline]
    fn u(&self) -> Vec3 {
        self.1
    }

    #[inline]
    fn v(&self) -> Vec3 {
        self.1
    }

    #[inline]
    #[allow(dead_code)]
    fn w(&self) -> Vec3 {
        self.2
    }
}

impl Camera {
    pub fn new(
        look_from: Point3,
        look_at: Point3,
        view_up: Vec3,
        vertical_field_of_view_in_degrees: f64,
        aspect_ratio: f64,
        aperture: f64,
        focus_distance: f64,
    ) -> Self {
        let theta = vertical_field_of_view_in_degrees.to_radians();
        let h = (theta / 2.0).tan();
        let viewport_height = 2.0 * h;
        let viewport_width = aspect_ratio * viewport_height;

        let w = (look_from - look_at).unit_vector();
        let u = view_up.cross(&w).unit_vector();
        let v = w.cross(&u);

        let origin = look_from;
        let horizontal = focus_distance * viewport_width * u;
        let vertical = focus_distance * viewport_height * v;
        let lower_left_corner = origin - horizontal / 2.0 - vertical / 2.0 - focus_distance * w;
        let lens_radius = aperture / 2.0;
        let basis = OrthonormalBasis(u, v, w);

        Camera {
            origin,
            lower_left_corner,
            horizontal,
            vertical,
            basis,
            lens_radius,
        }
    }

    pub fn get_ray(&self, s: f64, t: f64) -> Ray {
        let rd = self.lens_radius * Vec3::random_unit_in_disk();
        let offset = self.basis.u() * rd.x() + self.basis.v() * rd.y();

        Ray(
            self.origin + offset,
            self.lower_left_corner + s * self.horizontal + t * self.vertical - self.origin - offset,
        )
    }
}
